import React, { useState } from 'react';
import { StyleSheet, View, Button, ScrollView, FlatList } from 'react-native';
import GoalItem from './components/GoalItem'
import GoalInput from './components/GoalInput'

export default function App() {
  const [courseGoals, setCourseGoals] = useState([])
  const [visible, setVisible] = useState(false)
  const addGoalHandler = (enteredGoal) => {
    if (enteredGoal.length === 0) return;
    setCourseGoals(currentGoals => [...currentGoals, { id: Math.random().toString(), value: enteredGoal }])
    setVisible(false)
  }

  const removeGoalHandler = goalId => {
    setCourseGoals(currentGoals => currentGoals.filter(goal => goal.id !== goalId))
  }

  const onCancelAdditionalHandler = () => {
    setVisible(false)
  }
  return (
    <ScrollView>
      <View style={styles.container}>
        <Button title='Add New Goal' onPress={() => setVisible(true)} />
        <GoalInput
          addGoalHandler={addGoalHandler}
          visible={visible}
          cancelGoalHandler={onCancelAdditionalHandler}
        />
        <FlatList
          keyExtractor={(item) => item.id}
          data={courseGoals}
          renderItem={({ item }) => <GoalItem id={item.id} onDelete={removeGoalHandler} title={item.value} />}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 50
  },
});
