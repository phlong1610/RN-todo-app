import React, { useState } from 'react'
import { View, TextInput, Button, StyleSheet, Modal } from 'react-native'

const GoalInput = props => {
  const [enteredGoal, setEnteredGoal] = useState('')

  const onAddGoal = () => {
    props.addGoalHandler(enteredGoal)
    setEnteredGoal('')
  }

  const onCancel = () => {
    props.cancelGoalHandler()
    setEnteredGoal('')
  }
  return (
    <Modal visible={props.visible} animationType='slide'>
      <View style={styles.wrapper}>
        <TextInput
          placeholder="Enter course goal"
          style={styles.input}
          onChangeText={text => setEnteredGoal(text)}
          value={enteredGoal}
        />
        <View style={styles.buttonContainer}>
          <View style={styles.button}>
            <Button
              title="Cancel"
              color="red"
              onPress={onCancel}
            />
          </View>
          <View style={styles.button}>
            <Button
              title="Add item"
              onPress={onAddGoal}
            />
          </View>
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    // flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
    width: '80%',
    marginBottom: 10
  },
  buttonContainer: {
    flexDirection: 'row',
    width: '50%',
    justifyContent: 'space-around'
  },
  button: {
    width: '40%'
  }
})

export default GoalInput